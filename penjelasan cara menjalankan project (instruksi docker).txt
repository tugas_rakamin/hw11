penjelasan cara menjalankan project (instruksi docker)

1.Clone github
2.Buka folder menggunakan visual studio code
3.kemudian ketik perintah di terminal yaitu "npm install yarn" tunggu sampai proses selesai\
4.kemudian setelah selesai tambahkan pada unit test yaitu berupa program
// DELETE /api/todos/:id
describe("DELETE /api/todos/:id", () => {
  it("should return 200 ok", () => {
    request(app)
      .post("/api/todos")
      .set({
        title: "Lorem",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing",
      })
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(201);

    request(app)
      .delete("/api/todos/9")
      .expect("Content-Type", "application/json; charset=utf-8")
      .expect(200);
 });
});
5.Kemudian atur pada config.json antara lain: database dan username serta password sesuai dengan konfigurasi
6.kemudian setelah proses selesai ketik di terminal "npx sequelize db:migrate" fungsinya agar database ter integrasi ke dalam sistem kita
7.kemudian lakukan penginstalan untuk melakukan test unit yaitu "yarn add jest supertest"
8.kemudian lakukan inisialisasi pada jest yaitu npx jest --init, pilih konfigurasi sesuai dengan project kita
9.kemudian lakukan unit testing dengan cara menjalankan perintah pada terminal yaitu "npx yarn test"
10.Setelah unit testingnya passed kita tinggal menjalankan docker sebelum itu kita harus melakukan beberapa konfigurasi pada dockerfile dan docker-compose
untuk dockerfile konfigurasinya antara lain : FROM node:18-alpine
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
RUN yarn add sequelize-cli
COPY . .
EXPOSE 8081
CMD yarn start
dan untuk docker-compose lakukan konfigurasi sebagai berikut: version: "3"
services:
  web-server:
    image: todo-service-backend
    ports: 
      - "8081:8081"
    depends_on:
      - dbpgsql
  dbpgsql:
    image: postgres:12
    ports:
      - "5432:5432"
    env_file:
      - .env
    environment:
      - POSTGRES_USER
      - POSTGRES_PASSWORD
      - POSTGRES_DB
jangan lupa untuk menambahkan file (.env) pada root file dan lakukan konfigurasi sesuai database dengan contoh berikut ini
POSTGRES_USER=ian
POSTGRES_DB=movies_database
POSTGRES_PASSWORD=12345

Kemudian jangan lupa instal docker 

11.Setelah docker sudah install saatnya menjalakankan perintah pada terminal "docker build -t todo-service-backend . "
12.Kemudian setelah melakukan docker build lakukan perintah pada terminal sebagai berikut: docker-compose run --rm web-server npx sequelize-cli db:migrate  
13.kemudian lakukan perintah pada terminal "docker-compose up"
14.Setelah itu hit api pada postman sesuai dengan api yang kita buat contohnya seperti lakukan pada POST: localhost:8081/api/todos dan isi body pada menu raw dan jangan lupa ubah text menjadi json sehingga contohnya sebagai berikut 
{
    "title" : "Spiderman",
    "description" : "Action"
}
maka setelah di hit akan muncul seperti berikut 
    "data": {
        "id": 1,
        "title": "Spiderman",
        "description": "Action",
        "updatedAt": "2023-05-26T17:23:00.307Z",
        "createdAt": "2023-05-26T17:23:00.307Z"
    }

15.maka docker sudah berhasil jalan
16.kemudian pada git kita lakukan push agar terbentuk ci/cd yang sudah dibuat dalam program
stepnya sebagai berikut
-git init
-git remote add origin https://gitlab.com/tugas_rakamin/hw11.git 
-git add .  
-git commit -m "hw11"  
-git checkout main   
-git push -u origin main  
17.Kemudian cek pada website gitlab dengan menu ci/cd kemudian pilih pipe line
18.Setelah itu CI CD akan terotomasi berhenti sampai dengna status yang sudah dibuat pada program
